package INF102.lab1.triplicate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MyTriplicate<T extends Comparable<T>> implements ITriplicate<T> {

    @Override
    public T findTriplicate(List<T> list) {
        T triplicate = null;
        List<T> copiedList = new ArrayList<>(list);
        Collections.sort(copiedList);
        for (int i = 0; i < copiedList.size() - 2; i++) {
            T first = copiedList.get(i);
            T second = copiedList.get(i + 1);
            T third = copiedList.get(i + 2);

            if (first.equals(second) && second.equals(third)) {
                triplicate = first;
            }
        }
        return triplicate;
    }

}